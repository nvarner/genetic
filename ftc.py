from Box2D import b2World, b2PolygonShape

time_step = 1.0 / 60
velocity_iteration = 10
position_iteration = 10

world = b2World(gravity=(0, 0))
ground = world.CreateStaticBody(
    position=(0, -10),
    shapes=b2PolygonShape(box=(50, 10))
)

body = world.CreateDynamicBody(position=(0, 4))
box = body.CreatePolygonFixture(box=(1, 1), density=1, friction=0.3)

for i in range(60):
    world.Step(time_step, velocity_iteration, position_iteration)
    world.ClearForces()
    print(body.position, body.angle)
