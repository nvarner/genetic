from neural_network.NeuronLayer import NeuronLayer


class NeuralNetwork:
    def __init__(self, num_inputs, num_outputs, num_hidden_layers, neurons_per_hidden_layer):
        self.num_inputs = num_inputs
        self.num_outputs = num_outputs
        self.layers = []
        self.inputs = []
        self.render_data = {
            "neurons_have_data": False
        }

        if num_hidden_layers > 0:
            # Create the first hidden layer
            self.layers.append(NeuronLayer(neurons_per_hidden_layer, num_inputs))

            # Create all but the first hidden layer
            for i in range(num_hidden_layers - 1):
                self.layers.append(NeuronLayer(neurons_per_hidden_layer, neurons_per_hidden_layer))

            # Output layer
            self.layers.append(NeuronLayer(num_outputs, neurons_per_hidden_layer))
        else:
            # Create output layer
            self.layers.append(NeuronLayer(num_outputs, num_inputs))

    def get_weights(self) -> list:
        weights = []
        for layer in self.layers:
            for neuron in layer.neurons:
                for i in range(len(neuron.weights)):
                    weights.append(neuron.weights[i])
        return weights

    def put_weights(self, weights: list):
        weight = 0
        for layer in self.layers:
            for neuron in layer.neurons:
                for i in range(len(neuron.weights)):
                    neuron.weights[i] = weights[weight]
                    weight += 1

    def update(self, inputs: list) -> list:
        self.inputs = inputs
        outputs = []
        if len(inputs) != self.num_inputs:
            return outputs

        for i in range(len(self.layers)):
            if i > 0:
                inputs = outputs  # Update the inputs to the next layer

            outputs = self.layers[i].update(inputs)
        return outputs

    @staticmethod
    def calc_num_weights(num_inputs, num_outputs, num_hidden_layers, hidden_layer_size):
        first_to_second = (num_inputs + 1) * hidden_layer_size
        between_hidden = (num_hidden_layers - 1) * (hidden_layer_size + 1) ** 2
        hidden_to_output = (hidden_layer_size + 1) * num_outputs
        return first_to_second + between_hidden + hidden_to_output
