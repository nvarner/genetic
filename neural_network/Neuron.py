import math
import random

from neural_network import params


class Neuron:
    def __init__(self, num_inputs):
        self.weights = self.init_random_weights(num_inputs + 1)  # The extra one is for the bias
        self.output = 0  # This will store the last output so it can be rendered
        self.render_data = {}

    def activate(self, inputs):
        weight = 0
        net_input = 0

        # Iterate over each element except the bias
        for j in range(len(self.weights) - 1):
            net_input += self.weights[j] * inputs[weight]  # Add the weight times the input
            weight += 1

        # Add the bias
        net_input += self.weights[-1] * params.bias

        self.output = self.sigmoid(net_input, params.activation_response)
        return self.output

    @staticmethod
    def init_random_weights(num_inputs):
        weights = []
        for i in range(num_inputs):
            weights.append((random.random() * 2) - 1)  # Weights between -1 and 1
        return weights

    @staticmethod
    def sigmoid(activation, response):
        return 1 / (1 + math.pow(math.e, -activation / response))
