import random

from neural_network.Neuron import Neuron

index = 0


class Chromosome:
    def __init__(self, num_weights=-1, weights=None):
        """Be sure to define either num_weights or weights."""
        if weights is None:
            self.weights = Neuron.init_random_weights(num_weights)
        else:
            self.weights = weights
        self.fitness = 0
        global index
        self.index = index
        index += 1

    def __str__(self):
        return "I: " + str(self.index) + ", Fitness: " + str(self.fitness) + ", Weights: " + str(self.weights)

    def mutate(self, rate):
        for i in range(len(self.weights)):
            if random.random() < rate:
                # Add or subtract a small amount
                self.weights[i] += (0.1 * (random.random() - 0.5))

    @staticmethod
    def crossover(parent1, parent2):
        if len(parent1.weights) != len(parent2.weights):
            return

        child_weights = parent1.weights.copy()

        for i in range(len(child_weights)):
            if random.random() < 0.1:
                child_weights[i] = parent2.weights[i]

        result = Chromosome(weights=child_weights)
        result.mutate(0.2)

        return result

    @staticmethod
    def create_chromosome_list(list_length, num_weights=None):
        result = []
        for i in range(list_length):
            result.append(Chromosome(num_weights))
        return result
