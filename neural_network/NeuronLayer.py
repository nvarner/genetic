from neural_network.Neuron import Neuron


class NeuronLayer:
    def __init__(self, num_neurons, num_inputs_per_neuron):
        self.neurons = []
        for i in range(num_neurons):
            self.neurons.append(Neuron(num_inputs_per_neuron))

    def update(self, inputs: list) -> list:
        outputs = []

        # Iterate over every neuron, sum its inputs times weights, and activate it
        for neuron in self.neurons:
            # Activate the neuron, add it to the outputs
            outputs.append(neuron.activate(inputs))
        return outputs
