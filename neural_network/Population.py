import random

from neural_network.Chromosome import Chromosome


class Population:
    def __init__(self, individuals: list, max_survival_odds=1, min_survival_odds=0):
        self.individuals = individuals  # The individuals are Chromosomes

        # The slope of the line used for interpolation
        self.survival_slope = (max_survival_odds - min_survival_odds) / len(self.individuals)
        # The intercept of the line
        self.survival_intercept = min_survival_odds

    def sort_by_fitness(self):
        self.individuals.sort(key=lambda x: x.fitness, reverse=True)

    def die_and_reproduce(self):
        num_individuals = len(self.individuals)

        new_individuals = []
        while len(new_individuals) == 0:
            # Make sure that there is at least one individual left (although 2 or more is best for reproduction)
            new_individuals = []
            for i in range(num_individuals):
                # Go through each individual, decide if it may be kept or must be removed
                survival_odds = self.survival_odds(i)
                if random.random() < survival_odds:  # Includes 0, excludes 1, so less than
                    new_individuals.append(self.individuals[i])

        while num_individuals - len(new_individuals) > 0:
            # Choose 2 parents, make a child, add the child
            parent1 = random.choice(new_individuals)
            parent2 = random.choice(new_individuals)
            new_individuals.append(Chromosome.crossover(parent1, parent2))
            # new_individuals.append(Chromosome.crossover(parent1, parent1))  # Asexual reproduction

        self.individuals = new_individuals

    def survival_odds(self, position):
        # Invert position, because otherwise position 0 would have no chance
        return self.survival_slope * (len(self.individuals) - position) + self.survival_intercept

    def best_fitness(self, presorted=True):
        if not presorted:
            self.sort_by_fitness()
        return self.individuals[0].fitness

    def worst_fitness(self, presorted=True):
        if not presorted:
            self.sort_by_fitness()
        return self.individuals[-1].fitness

    def average_fitness(self):
        total = 0
        for chromosome in self.individuals:
            total += chromosome.fitness
        return total / len(self.individuals)

    def __str__(self):
        result = ""
        for i in self.individuals:
            result += str(i) + "\n"
        return result[:-1]


if __name__ == "__main__":
    population = Population(Chromosome.create_chromosome_list(10))
    population.sort_by_fitness()
    population.die_and_reproduce()
