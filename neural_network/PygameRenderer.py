import pygame

from neural_network.NeuralNetwork import NeuralNetwork
from neural_network.NeuronLayer import NeuronLayer


class PygameRenderer:
    def __init__(self, width=640, height=480, margin=10):
        pygame.init()
        self.screen = pygame.display.set_mode((width, height))
        self.background = pygame.Surface(self.screen.get_size())
        self.background.fill((255, 255, 255))
        self.background = self.background.convert()
        self.screen.blit(self.background, (0, 0))
        self.margin = margin
        self.radius = 30
        self.font = pygame.font.SysFont("monospace", 15)

    def render(self, neural_network: NeuralNetwork):
        # Watch for events
        for event in pygame.event.get():
            if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                self.quit()
                return

        # If the neurons don't have cached render data, generate it
        if not neural_network.render_data["neurons_have_data"]:
            width = self.screen.get_size()[0]
            height = self.screen.get_size()[1]
            self.generate_render_data(neural_network, width, height, self.margin, self.radius)

        # Update the values the inputs will display
        for i in range(len(neural_network.inputs)):
            neural_network.render_data["input_layer"].neurons[i].output = neural_network.inputs[i]

        self.draw_connections(neural_network)
        self.draw_neurons(neural_network)

        pygame.display.flip()

    def draw_connections(self, neural_network: NeuralNetwork):
        last_layer = []
        this_layer = []
        # Input layer
        for neuron in neural_network.render_data["input_layer"].neurons:
            last_layer.append(neuron.render_data["position"])
        # Most layers
        for layer in neural_network.layers:
            for neuron in layer.neurons:
                this_layer.append(neuron.render_data["position"])

            for neuron in last_layer:
                for neuron2 in this_layer:
                    pygame.draw.line(self.screen, (0, 0, 0), neuron, neuron2)

            last_layer = this_layer
            this_layer = []

    def draw_neurons(self, neural_network: NeuralNetwork):
        # Input layer
        for neuron in neural_network.render_data["input_layer"].neurons:
            self.draw_neuron(neuron)
        # Most layers
        for layer in neural_network.layers:
            for neuron in layer.neurons:
                self.draw_neuron(neuron)

    def draw_neuron(self, neuron):
        position = neuron.render_data["position"]

        # Draw the neuron
        pygame.draw.circle(self.screen, (255 * min(abs(neuron.output), 1), 0, 0), position, self.radius)

        # Draw the number inside the neuron
        output_string = "{:.3f}".format(neuron.output)
        number_size = self.font.size(output_string)
        text_position = (
            position[0] - (number_size[0] // 2),
            position[1] - (number_size[1] // 2)
        )
        output = self.font.render(output_string, 1, (255, 255, 255))
        self.screen.blit(output, text_position)

    @staticmethod
    def generate_render_data(neural_network: NeuralNetwork, width: int, height: int, margin: int, radius: int):
        # Set up constants used to draw each layer
        num_layers = len(neural_network.layers) + 1  # Number of layers in network, including inputs
        usable_width = width - (2 * (margin + radius))  # How much space can be taken up
        width_spacing = int(usable_width / (num_layers - 1))  # How far apart each neuron must be

        # Input layer
        input_layer = NeuronLayer(neural_network.num_inputs, 0)
        PygameRenderer.generate_render_data_layer(input_layer, height, margin, radius, width_spacing, 0)
        neural_network.render_data["input_layer"] = input_layer

        # Hidden layer/output layers
        for i in range(1, num_layers):
            layer = neural_network.layers[i - 1]
            PygameRenderer.generate_render_data_layer(layer, height, margin, radius, width_spacing, i)

        neural_network.render_data["neurons_have_data"] = True

    @staticmethod
    def generate_render_data_layer(layer, height, margin, radius, width_spacing, index):
        num_neurons = len(layer.neurons)
        usable_height = height - (2 * (margin + radius))
        height_spacing = -1 if num_neurons == 1 else int(usable_height / (num_neurons - 1))

        if num_neurons > 1:  # For multiple neurons in one layer, this code will be correct
            for j in range(num_neurons):
                neuron = layer.neurons[j]
                neuron.render_data["position"] = (
                    (index * width_spacing) + margin + radius,
                    (j * height_spacing) + margin + radius
                )
        else:  # With only one neuron in a layer, the above code does not work
            neuron = layer.neurons[0]
            neuron.render_data["position"] = (
                (index * width_spacing) + margin + radius,
                height // 2
            )

    @staticmethod
    def quit():
        pygame.quit()


if __name__ == "__main__":
    import time

    renderer = PygameRenderer()
    network = NeuralNetwork(2, 2, 2, 1)
    renderer.render(network)

    while True:
        time.sleep(0.1)
