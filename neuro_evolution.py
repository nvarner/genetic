import gym

import matplotlib.pyplot as plt
import numpy as np

from neural_network.Chromosome import Chromosome
from neural_network.NeuralNetwork import NeuralNetwork
from neural_network.Population import Population
from neural_network.PygameRenderer import PygameRenderer

inputs = 6
outputs = 3
hidden_layers = 2
hidden_layer_size = 5
num_weights = NeuralNetwork.calc_num_weights(inputs, outputs, hidden_layers, hidden_layer_size)

pop_size = 20
generations = 50

render = True

top_scores = []
mean_scores = []
low_scores = []

population = Population(Chromosome.create_chromosome_list(pop_size, num_weights))

# The same network can be reused by setting the weights based on the chromosome
network = NeuralNetwork(inputs, outputs, hidden_layers, hidden_layer_size)
renderer = None if not render else PygameRenderer()

env = gym.make("Acrobot-v1")

for generation in range(generations):
    for i_episode in range(pop_size):
        population.individuals[i_episode].fitness = 0
        network.put_weights(population.individuals[i_episode].weights)
        observation = env.reset()

        network_result = network.update(observation)
        result = 0 if network_result[0] > network_result[1] and network_result[0] > network_result[2] else\
            1 if network_result[1] > network_result[2] else 2

        for t in range(200):
            if render or generation == generations - 1:
                env.render()
            observation, reward, done, info = env.step(result)

            network_result = network.update(observation)
            result = 0 if network_result[0] > network_result[1] and network_result[0] > network_result[2] else \
                1 if network_result[1] > network_result[2] else 2

            population.individuals[i_episode].fitness += reward
            if render:
                renderer.render(network)
            if done:
                break

    population.sort_by_fitness()

    print(population)

    best_fitness = population.best_fitness()
    worst_fitness = population.worst_fitness()
    average_fitness = population.average_fitness()

    print("Generation: " + str(generation + 1) + "/" + str(generations))
    print("Best fitness: " + str(best_fitness))
    print("Worst fitness: " + str(worst_fitness))
    print()

    population.die_and_reproduce()

    top_scores.append(best_fitness)
    mean_scores.append(average_fitness)
    low_scores.append(worst_fitness)

env.close()
if render:
    renderer.quit()

# Plot the results
plt.plot(range(generations), mean_scores, label="Average fitness")
plt.plot(range(generations), top_scores, label="Maximum fitness")
plt.plot(range(generations), low_scores, label="Minimum fitness")
plt.legend()
plt.title("Evolution")
plt.xlabel("Generations")
plt.ylabel("Fitness")
plt.show()
