from gym.envs import register

register(
    id="BasicSoccer-v0",
    entry_point="gym_ftc.envs:BasicSoccerEnv"
)
