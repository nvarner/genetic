import gym
import pygame
from Box2D import b2PolygonShape, b2World, b2_staticBody, b2_dynamicBody

FPS = 50
TIME_STEP = 1.0 / FPS
PPM = 10  # Pixels Per Meter

FRAME_SIDE = 1000


class BasicSoccerEnv(gym.Env):
    metadata = {
        "render.modes": ["human"],
        "video.frames_per_second": FPS
    }

    def __init__(self):
        # Pygame
        self.clock = pygame.time.Clock()
        self.screen = pygame.display.set_mode((FRAME_SIDE, FRAME_SIDE), 0, 32)
        pygame.display.set_caption("FTC Soccer")
        self.colors = {
            b2_staticBody: (255, 255, 255, 255),
            b2_dynamicBody: (127, 127, 127, 255),
        }

        # Box2d
        self.dimensions = (50, 50)
        self.world = b2World(gravity=(0, -10), doSleep=True)
        self.walls = {
            "bottom": self.world.CreateStaticBody(
                position=(self.dimensions[0], -2.5),
                shapes=b2PolygonShape(box=(self.dimensions[0], 5))
            ),
            "top": self.world.CreateStaticBody(
                position=(self.dimensions[0], 2 * self.dimensions[1]),
                shapes=b2PolygonShape(box=(self.dimensions[0], 5))
            # ),
            # "left": self.world.CreateStaticBody(
            #     position=(-5, 0),
            #     shapes=b2PolygonShape(box=(5, self.dimensions[1]))
            # ),
            # "right": self.world.CreateStaticBody(
            #     position=(self.dimensions[0] - 5, 0),
            #     shapes=b2PolygonShape(box=(5, self.dimensions[1]))
            )
        }
        self.dynamic = self.world.CreateDynamicBody(
            position=(self.dimensions[0] / 2, self.dimensions[1] / 2)
        )

    def step(self, action):
        self.world.Step(TIME_STEP, 10, 10)

    def reset(self):
        pass

    def render(self, mode="human"):
        self.screen.fill((0, 0, 0, 0))

        for body in self.world.bodies:
            for fixture in body.fixtures:
                shape = fixture.shape
                vertices = [(body.transform * v) * PPM for v in shape.vertices]
                vertices = [(v[0], FRAME_SIDE - v[1]) for v in vertices]

                pygame.draw.polygon(self.screen, self.colors[body.type], vertices)

        pygame.display.flip()
        self.clock.tick(FPS)


if __name__ == "__main__":
    env = BasicSoccerEnv()
    env.step(0)
    env.render()
    while True:
        pass
